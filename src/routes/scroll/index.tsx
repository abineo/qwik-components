import { component$ } from "@builder.io/qwik";
import { Layout, Link } from "$lib";
import { Footer } from "$components/Footer";

export default component$(() => {
	return (
		<Layout>
			<section>
				<Link href="/">Go to Home</Link>

				<h1>Hello, world!</h1>
				{Array.from(Array(100)).map((i) => (
					<p key={i}>Hello, world!</p>
				))}
			</section>

			<div q:slot="footer">
				<Footer />
			</div>
		</Layout>
	);
});
