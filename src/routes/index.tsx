import { component$ } from "@builder.io/qwik";
import { Layout, Link } from "$lib";
import { Footer } from "$components/Footer";

export default component$(() => {
	return (
		<Layout>
			<Link href="/scroll">Go to Scroll</Link>

			<section>
				<h1>Hello, world!</h1>
				<div>
					<Link style="button" href="https://abineo.swiss" newTab>
						Go to abineo.swiss
					</Link>
				</div>
				<br />
				<br />
				<Link style="outline">
					<span>foo</span>
					<span>bar</span>
					<span>baz</span>
				</Link>
			</section>

			<div q:slot="footer">
				<Footer />
			</div>
		</Layout>
	);
});
