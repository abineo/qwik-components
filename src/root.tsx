import { component$ } from "@builder.io/qwik";
import { QwikCityProvider, RouterOutlet } from "@builder.io/qwik-city";

import "./global.scss";

export default component$(() => {
	return (
		<QwikCityProvider>
			<head>
				<meta charSet="utf-8" />
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0"
				/>
				<link
					rel="icon"
					type="image/svg+xml"
					href={buildPath(import.meta.env.BASE_URL, "favicon.svg")}
				/>
				<title>Qwik Components | Abineo</title>
			</head>
			<body>
				<RouterOutlet />
			</body>
		</QwikCityProvider>
	);
});

function buildPath(...segments: string[]): string {
	let path = "/" + segments.join("/");
	while (path.includes("//")) {
		path = path.replaceAll("//", "/");
	}
	return path;
}
