import { component$ } from "@builder.io/qwik";

const timestamp = new Date().toISOString();

export const Footer = component$(() => {
	return (
		<section>
			<code>
				<strong>
					{import.meta.env.PUBLIC_VERSION || "0.0.0-local"}
				</strong>
				&nbsp;@&nbsp;
				<small>
					<time dateTime={timestamp}>{timestamp}</time>
				</small>
			</code>
		</section>
	);
});
