import { Slot, component$, useStylesScoped$ } from "@builder.io/qwik";
import css from "./Layout.scss?inline";

export type LayoutProps = {};

export const Layout = component$<LayoutProps>(() => {
	useStylesScoped$(css);
	return (
		<>
			<main>
				<div class="body">
					<Slot />
				</div>
				<footer>
					<Slot name="footer" />
				</footer>
			</main>
		</>
	);
});
