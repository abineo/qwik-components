import { Slot, component$, useStylesScoped$ } from "@builder.io/qwik";
import css from "./Link.scss?inline";
import { Link as QwikLink } from "@builder.io/qwik-city";

export type LinkProps = {
	href?: string;
	newTab?: boolean;
	style?: "link" | "button" | "outline";
};

export const Link = component$<LinkProps>((props) => {
	useStylesScoped$(css);
	return (
		<>
			<QwikLink
				href={props.href}
				rel="noopener"
				target={props.newTab ? "_blank" : undefined}
			>
				<span
					class={{
						link: !props.style || props.style === "link",
						button: props.style === "button",
						outline: props.style === "outline",
					}}
				>
					<Slot />
				</span>
			</QwikLink>
		</>
	);
});
