import { extendConfig } from "@builder.io/qwik-city/vite";
import { staticAdapter } from "@builder.io/qwik-city/adapters/static/vite";
import { default as baseConfig } from "../../vite.config";

const origin = process.env.ORIGIN || "http://localhost:5173/";

export default extendConfig(baseConfig, () => {
	return {
		build: {
			ssr: true,
			rollupOptions: {
				input: ["@qwik-city-plan"],
			},
		},
		plugins: [staticAdapter({ origin })],
	};
});
