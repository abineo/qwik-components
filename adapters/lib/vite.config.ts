import type { RollupOptions } from "rollup";
import { extendConfig } from "@builder.io/qwik-city/vite";
import { default as baseConfig } from "../../vite.config";
import pkg from "../../package.json";

// externalize dependencies that shouldn't be bundled into the library
const { dependencies = {}, peerDependencies = {} } = pkg as any;
const makeRegex = (dep) => new RegExp(`^${dep}(/.*)?$`);
const excludeAll = (obj) => Object.keys(obj).map(makeRegex);
const rollupOptions: RollupOptions = {
	external: [
		/^node:.*/,
		...excludeAll(dependencies),
		...excludeAll(peerDependencies),
	],
};

export default extendConfig(baseConfig, () => {
	return {
		publicDir: ".i.dont.exist",
		build: {
			target: "es2020",
			lib: {
				entry: "./src/lib/index.ts",
				formats: ["es", "cjs"],
				fileName: (format) =>
					`index.qwik.${format === "es" ? "mjs" : "cjs"}`,
			},
			rollupOptions,
		},
	};
});
