const { name, version } = require("../package.json");
const apiUrl = process.env.CI_API_V4_URL;
const id = process.env.CI_PROJECT_ID;

const url = new URL(`${apiUrl}/projects/${id}/packages`);
url.search = new URLSearchParams({
	job_token: process.env.CI_JOB_TOKEN,
	order_by: "version",
	sort: "desc",
	package_name: name,
	package_version: version,
});

async function main(url) {
	const res = await fetch(url);
	const data = await res.json();
	console.info(data[0].id);
}

main(url);
