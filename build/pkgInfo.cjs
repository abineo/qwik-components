const { name, version } = require("../package.json");

if (process.argv[2] === "--print-name") console.log(name);
else if (process.argv[2] === "--print-version") console.log(version);
