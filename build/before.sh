#!/bin/sh
if [ ! $(pnpm -v) ]; then
    corepack enable
    corepack prepare pnpm@latest-8 --activate
    pnpm config set store-dir .pnpm-store
fi

if [ ! -d node_modules ]; then
    pnpm install
fi

if [ ! -e mo ]; then
    curl -sSL https://raw.githubusercontent.com/tests-always-included/mo/master/mo -o mo
    chmod +x mo
fi
