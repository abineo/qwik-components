# Abineo Qwik Components

[![Pipeline](https://gitlab.com/abineo/qwik-components/badges/next/pipeline.svg)](https://gitlab.com/abineo/qwik-components/-/pipelines)
[![Latest Release](https://gitlab.com/abineo/qwik-components/-/badges/release.svg)](https://gitlab.com/abineo/qwik-components/-/releases)
[![Static Badge](https://img.shields.io/badge/Documentation-%F0%9F%93%96-%23ddd)](https://qwik-components.abineo.dev/)

## Project Structure

```
src/
├── lib/
├── routes/
└── components/
```

-   `src/lib`: library components

-   `src/routes`: documentation pages

-   `src/components`: documentation components

## Development

Start dev server with

```
pnpm dev
```

Make shure to format the code (`pnpm fmt`).

## Component library

Build the component library with

```
pnpm build.lib
```

This uses vite's library mode with the config at `/adapters/lib/vite.config.ts`.  
Output is in `/lib`.

## Documentation

Generate documentation pages with

```
pnpm build.pages
```

This uses qwik's ssg adapter configured at `/adapters/static/vite.config.ts`.  
Output is in `/dist`.

## [License](./LICENSE.md)

<small>Copyright (C) 2024 Abineo AG</small>

<small>This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.</small>

<small>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.</small>

<small>You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses.</small>
